# 有序列表/無序列表

* 桃子
* 蘋果
* 香蕉

1. goog
1. msft
1. amd
1. dell

# 表格

| Column 1 | Column 2 | Column 3 |
| -------- | -------- | -------- |
| Text     | Text     | Text     |
| Text     | Text     | Text     |
| Text     | Text     | Text     |

# 鏈接

[到 https://www.google.com 的鏈接 會在新頁面打開](https://www.google.com)

[頁面跳轉 只能是 view 地址](home/0)

# 代碼 高亮/行號/檔案名

```
package main

import "fmt"

func main() {
	fmt.Println("hello world")
}
```

```go
#info=false
package main

import "fmt"

func main() {
	fmt.Println("hello world")
}
```

```go
#info="main.go"
package main

import "fmt"

func main() {
	fmt.Println("hello world")
}
```
```go
#info={"name":"main.go","noline":true}
package main

import "fmt"

func main() {
	fmt.Println("hello world")
}
```

# 數據公式

\\(2 ^ {28}\\) 

$$> 2 ^ {56}$$ 

