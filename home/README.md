# book-web

歡迎選用 book-web 來構建你的 數據中心

現在 你可以 自由的通過 markdown 在線創建你的 數據並分銷給 全世界

# LICENS
book-web 是 **GPL3** 的開源項目 你可以 [到此](https://gitlab.com/king011/book-web) 獲取 源碼和 更詳細的 使用說明

# 注意

book-web 保持**開放共享**的精神 所以沒有設置 權限系統 僅設置一個管理員用於數據更新 管理員更新到上面的數據 可以被任何人查看

如果你是個自私的**蟑螂** 請不要使用 book-web 以免你的 **骯髒數據** 被泄漏到網路